var webpackConfig = require('./webpack.config');
webpackConfig.devtool = 'inline-source-map';

module.exports = function (config) {
    config.set({
        browsers: [ 'Chrome' ],
        singleRun: true,
        frameworks: [ 'mocha' ],
        files: [
            'webpack.tests.js'
        ],
        plugins: [
            'karma-chrome-launcher',
            'karma-mocha',
            'karma-sourcemap-loader',
            'karma-mocha-reporter',
            'karma-webpack'
        ],
        preprocessors: {
            'webpack.tests.js': [ 'webpack', 'sourcemap' ]
        },
        reporters: [ 'mocha' ],
        webpack: webpackConfig,
        webpackServer: {
            noInfo: true
        }
    });
};