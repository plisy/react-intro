var Webpack = require('webpack');
// we are not pigs, we do valid paths even for poor windows guys!
var Path = require('path');


module.exports = {
    // there are different ways to add source maps but this seems to work best
    devtool: 'cheap-module-eval-source-map',

    // set what files to use to start building bundle
    entry: [
        // add the client script for hot-reload
        'webpack-hot-middleware/client',
        // app main file
        Path.join(__dirname, 'src', 'index.js')
    ],
    output: {
        path: Path.join(__dirname, 'dist'),
        filename: 'bundle.js',
        publicPath: '/dist/'
    },

    plugins: [
        new Webpack.HotModuleReplacementPlugin(),
        // even when build fails, webpack watch should not die
        new Webpack.NoErrorsPlugin()
    ],

    // and now the fun part, compilers
    module: {
        loaders: [{
            // what files should be loaded using this loader
            test: /\.js/,
            loaders: ['babel'], // this will get better (& way more messy), trust me
            include: Path.join(__dirname, 'src')
        }]
    }
};
