import React from 'react';
import ReactDOM from 'react-dom';

import Hello from './components/Hello';


var mountNode = document.getElementById('app');
// Use ReactDOM to render our component into mount node.
ReactDOM.render(<Hello name="Joe" />, mountNode);