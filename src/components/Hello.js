import React from 'react';


const Hello = ({name}) => (
    <div>
        Hello, <i>{name}</i>!
    </div>
);


export default Hello;