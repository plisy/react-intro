import React from 'react';
import ReactDOM from 'react-dom';

import expect from 'expect';
import TestUtils from 'react-addons-test-utils';

import Hello from '../Hello';


describe('Hello', () => {

    it('should display name', () => {

        const d = document.createElement('div');

        ReactDOM.render(
            <Hello name="Joe"/>,
            d
        );

        expect(d.innerText).toEqual('Hello, Joe!');

    });

    const Clickable = React.createClass({
        propTypes: {
            handleClick: React.PropTypes.func.isRequired
        },
        render () {
            const {handleClick} = this.props;
            return <button onClick={handleClick}>I'm clickable</button>
        }
    });

    it('should be clickable', (done) => {

        const res = TestUtils.renderIntoDocument(
            <Clickable handleClick={() => done()}/>
        );

        TestUtils.Simulate.click(ReactDOM.findDOMNode(res));

    });

    const Incrementer = React.createClass({
        render () {
            return <button onClick={this.props.handleIncrement}>Increment</button>
        }
    });

    const Counter = React.createClass({
        render () {
            return <div>{this.props.count}</div>;
        }
    });

    const CounterContainer = React.createClass({
        getInitialState () {
            return {count: 0};
        },

        handleIncrement () {
            console.log('incrementing state');
            this.setState({count: this.state.count + 1})
        },

        render () {
            return (
                <div>
                    <Incrementer ref={i=> this.incrementer = i} handleIncrement={this.handleIncrement}/>

                    <Counter count={this.state.count} ref={i=> this.counter = i}/>
                </div>
            )
        }
    });

    it('should increment count on click', () => {

        const res = TestUtils.renderIntoDocument(<CounterContainer />);

        const incrementer = TestUtils.findRenderedComponentWithType(res, Incrementer);

        TestUtils.Simulate.click(ReactDOM.findDOMNode(incrementer));
        const counter = TestUtils.findRenderedComponentWithType(res, Counter);

        expect(counter.props.count).toEqual(1);

    });


});